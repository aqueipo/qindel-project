// Creamos el router
var app = new AppRouter();
// Creamos la barra de navegación
new NavBar({
	el : document.getElementById('nav-item-container'),
	doc: document.getElementById('nav-item-documentacion')
});
// Iniciamos el gestor de histórico
Backbone.history.start();
