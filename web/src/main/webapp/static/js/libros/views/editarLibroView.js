/**
 * Esta "clase" se encarga de representar la vista de un diálogo modal de edición
 * de libros. Muestra el libro "libro" y se encarga de capturar los eventos de
 * edición para actualizarlo de forma.
 */
var EditarLibroView = Backbone.View.extend({

	/**
	 * Punto de enganche en el árbol dom
	 */
	el : $('#mensajes'),
	modal : '#editarLibro',

	/**
	 * Libro mostrado y (posiblemente) actualizado.
	 */
	libro : null,

	/**
	 * Plantilla de libro
	 */
	libroTemplate: null,

	/**
	 * Lista de eventos
	 */
	events : {
		"hidden.bs.modal #editarLibro" : "destroy",
		"click #aceptar-edit" : "saveEdit"
	},

	/**
	 * Incializa las instancias estableciendo la plantilla de este view.
	 *
	 * @param options.
	 *            Es un hash con un único elemento {editarLibroTemplate: plantilla}
	 */
	initialize : function(options) {
		this.template = options.editarLibroTemplate;
		this.libro = options.libro;
		this.coleccionTemplate = options.coleccionTemplate;
	},

	/**
	 * Crea un nodo dom con la plantilla html y los datos del país establecido.
	 *
	 * @return Devuelve la instancia sobre la que se ejecuta la función.
	 */
	render : function() {
		this.$el.html(this.template(this.libro.toJSON()));
		$(this.modal).modal();
		return this;
	},

	/**
	 * Función que vacía el
	 */
	hide : function() {
		this.$el.empty();
	},

	/**
	 * Función que guarda los cambios hechos en el libro
	 */
	saveEdit: function() {
		// Cargamos los nuevos valores
		this.libro.set({
            titulo: $('#titulo').val(),
            autor: $('#autor').val()
					});

		// Guardamos los cambios hechos
		this.libro.save({}, {
            success: function (model, respose, options) {
                console.log("El libro ha sido actualizado en el server");
            },
            error: function (model, xhr, options) {
                console.log("Error actualizando el modelo");
            }
        });

		$(this.modal).modal('hide');
	},

	/**
	 * Función que elimina el modal y sus eventos
	 */
	destroy : function() {
		this.stopListening();
		this.undelegateEvents();
		this.$el.removeData();
		$(this.modal).remove();
	}
});
