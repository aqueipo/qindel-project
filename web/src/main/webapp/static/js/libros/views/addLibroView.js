/**
 * Esta "clase" se encarga de representar la vista de un diálogo modal de
 * creación de libros. Muestra el libro "libro" y se encarga de capturar los
 * eventos de edición para actualizarlo de forma.
 */
var AddLibroView = Backbone.View.extend({

	/**
	 * Punto de enganche en el árbol dom
	 */
	el : $('#mensajes'),
	modal : '#addLibro',

	/**
	 * Libro mostrado y (posiblemente) actualizado.
	 */
	libro : null,

  /**
   * Plantilla de libro
   */
	libroTemplate: null,

  /**
   * Colección de libros
   */
  libroCollection: null,

  /**
   * Lista de eventos
   */
	events : {
		"hidden.bs.modal #addLibro" : "destroy",
		"click #aceptar-add" : "saveAdd"
	},

	/**
	 * Incializa las instancias estableciendo la plantilla de este view.
	 *
	 * @param options.
	 *            Es un hash con un único elemento {editarLibroTemplate: plantilla}
	 */
	initialize : function(options) {
		this.template = options.addLibroTemplate;
    this.libroCollection = options.libroCollection;
	},

	/**
	 * Crea un nodo dom con la plantilla html y los datos del país establecido.
	 *
	 * @return Devuelve la instancia sobre la que se ejecuta la función.
	 */
	render : function() {
		this.$el.html(this.template());
		$(this.modal).modal();
		return this;
	},

  /**
	 * Función que vacía el
	 */
	hide : function() {
		this.$el.empty();
	},

  /**
	 * Función que intenta crear el nuevo libro
	 */
	saveAdd: function() {
    // Creamos un nuevo libro
    var newLibro = new LibroModel({
      titulo: $('#titulo').val(),
      autor: $('#autor').val()
     });
    var that = this;
    
		// Guardamos el libro
		newLibro.save({}, {
            success: function (model, respose, options) {
                console.log("El libro ha sido creado en el server");
                that.libroCollection.reset();
            },
            error: function (model, xhr, options) {
                console.log("Error creando el modelo");
            }
        });

		$(this.modal).modal('hide');
	},

  /**
	 * Función que elimina el modal y sus eventos
	 */
	destroy : function() {
		this.stopListening();
		this.undelegateEvents();
		this.$el.removeData();
		$(this.modal).remove();
	}
});
