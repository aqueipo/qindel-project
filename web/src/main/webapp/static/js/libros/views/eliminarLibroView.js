/**
 * Esta "clase" se encarga de representar la vista de un diálogo modal de eliminación de libros. Muestra un libro
 * "libro" y se encarga de capturar los eventos de borrado y eliminarlo de forma efectiva.
 */
var EliminarLibroView = Backbone.View.extend({

	/**
	 * Punto de enganche en el árbol dom
	 */
	el : $('#mensajes'),
	modal : '#eliminarLibro',

	/**
	 * Libro mostrado y (posiblemente) borrado.
	 */
	libro : null,

	events : {
		"hidden.bs.modal #eliminarLibro" : "destroy",
		"click #aceptar-delete" : "delete"
	},

	/**
	 * Incializa las instancias estableciendo la plantilla de este view.
	 *
	 * @param options.
	 *            Es un hash con un único elemento {eliminarLibroTemplate: plantilla}
	 */
	initialize : function(options) {
		this.template = options.eliminarLibroTemplate;
		this.libro = options.libro;
	},

	/**
	 * Crea un nodo dom con la plantilla html y los datos del país establecido.
	 *
	 * @return Devuelve la instancia sobre la que se ejecuta la función.
	 */
	render : function() {
		this.$el.html(this.template(this.libro.toJSON()));
		$(this.modal).modal('show');
		return this;
	},

	/**
	* Función que oculta el elemento dom
	*/
	hide : function() {
		this.$el.empty();
	},

	/**
	 * Función que borra un libro de la colección
	 */
	delete : function() {
		var _collection = this.libro.collection;
		var _model = this.libro;
		var _that = this;
		this.libro.destroy({
			contentType : 'application/json',
			dataType : 'text',
			success: function (model, respose, options) {
				console.log("El libro ha sido destruido en el server");
				_collection.remove(_model);
				_collection.reset();
			},
			error: function (model, xhr, options) {
				console.log("Error actualizando el modelo");
			}
		});
		$(this.modal).modal('hide');
	},

	destroy : function() {
		this.stopListening();
		this.undelegateEvents();
		this.$el.removeData();
		$(this.modal).remove();
	}
});
