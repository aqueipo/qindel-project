/**
 * Router que se encarga de manejar las peticiones a la página de inicio, de libros y de referencias.
 */
var AppRouter = Backbone.Router.extend({
	/**
	 * Rutas manejadas en la aplicación
	 */
	routes : {
		"" : "index",
		"libros" : "libros",
		"paises": "paises",
		"ciudades": "ciudades",
		"referencias" : "referencias",
		"documentacion": "documentacion"
	},
	/**
	 * Vista encargada de mostrar la página de inicio
	 */
	indexView : null,
	/**
	 * Vista encargada de mostrar la lista de libros
	 */
	libroCollectionView : null,
	/**
	 * Vista encargada de mostrar la lista de países
	 */
	paisCollectionView : null,
	/**
	* Vista encargada de mostrar la lista de ciudades
	*/
	paisCiudadSedeCollectionView: null,
	/**
	 * Vista encargada de mostrar la la página de referencias
	 */
	referenciasView : null,
	/**
	 * Vista encargada de mostrar la la página de referencias
	 */
	documentacionView : null,
	/**
	* Vista actual
	*/
	currentView : null,

	initialize : function() {
		var indexTemplate = this.loadTemplate('inicio', 'indexTemplate');
		this.indexView = new IndexView({
			template : indexTemplate
		});

		// Inicialización de libros
		var libroCollection = new LibroCollection();
		var libroCollectionTemplate = _.template(this.loadTemplate('libros', 'libroCollection'));
		var libroModelTemplate = _.template(this.loadTemplate('libros', 'libroModel'));
		var editarLibroTemplate = _.template(this.loadTemplate('libros', 'editarLibro'));
		var eliminarLibroTemplate = _.template(this.loadTemplate('libros', 'eliminarLibro'));
		var addLibroTemplate = _.template(this.loadTemplate('libros', 'addLibro'));
		this.libroCollectionView = new LibroCollectionView({
			collection : libroCollection,
			template : libroCollectionTemplate,
			libroModelTemplate : libroModelTemplate,
			editarLibroTemplate : editarLibroTemplate,
			eliminarLibroTemplate : eliminarLibroTemplate,
			addLibroTemplate : addLibroTemplate
		});

		// Inicializacion de paises
		var paisCollection = new PaisCollection();
		var paisCollectionTemplate = _.template(this.loadTemplate('paises', 'paisCollection'));
		var paisModelTemplate = _.template(this.loadTemplate('paises', 'paisModel'));
		this.paisCollectionView = new PaisCollectionView({
			collection : paisCollection,
			template: paisCollectionTemplate,
			paisModelTemplate : paisModelTemplate
		});

		// Inicialización de ciudades
		var paisCiudadSedeCollection = new PaisCiudadSedeCollection();
		var paisCiudadSedeCollectionTemplate = _.template(this.loadTemplate('paises', 'paisCiudadSedeCollection'));
		var paisCiudadSedeModelTemplate = _.template(this.loadTemplate('paises', 'paisCiudadSedeModel'));
		this.paisCiudadSedeCollectionView = new PaisCiudadSedeCollectionView({
			collection : paisCiudadSedeCollection,
			template: paisCiudadSedeCollectionTemplate,
			paisCiudadSedeModelTemplate : paisCiudadSedeModelTemplate
		});

		// Inicialización de referencias
		var referenciasTemplate = this.loadTemplate('referencias', 'referencias');
		this.referenciasView = new ReferenciasView({
			template : referenciasTemplate
		});

		// Inicialización de documentación
		var documentacionTemplate = this.loadTemplate('documentacion', 'documentacion');
		this.documentacionView = new DocumentacionView({
			template : documentacionTemplate
		});
	},

	/**
	 * Muestra la página de inicio
	 */
	index : function() {
		this.changeView(this.indexView);
	},

	/**
	 * Muestra la página de referencias.
	 */
	referencias : function() {
		this.changeView(this.referenciasView);
	},

	/**
	 * Muestra la página de documetación
	 */
	documentacion : function() {
		this.changeView(this.documentacionView);
	},

	/**
	 * Recupera la lista de libros y la muestra en una tabla.
	 */
	libros : function() {
		this.changeView(this.libroCollectionView);
	},

	/**
	 * Recupera la lista de paises en una tabla.
	 */
	 paises : function() {
		 this.changeView(this.paisCollectionView);
	 },

	 /**
		* Recupera la lista de paises en una tabla.
		*/
		ciudades : function() {
			this.changeView(this.paisCiudadSedeCollectionView);
		},

		/**
		* Responsable del cambio de vistas
		*/
		changeView : function(newView) {
			if (newView && this.currentView && newView != this.currentView) {
				this.currentView.hide();
				this.currentView = newView;
				this.currentView.render();
			} else if (newView) {
				this.currentView = newView;
				this.currentView.render();
			}
		},

		/**
		* Carga una plantilla html indicándole el nombre del archivo sin extensión.
		* La función buscará la plantilla en el directorio
		* web/static/js/libros/templates y utilizará como extensión del archivo ".tmpl"
		*/
		loadTemplate : function(feature, name) {
			var baseUrl = CONTEXT_PATH + '/static/js/' + feature + '/templates';
			var url = baseUrl + '/' + name + '.tmpl';

			var template = '';
			$.ajax({
				url : url,
				method : 'GET',
				async : false,
				dataType : 'html',
				success : function(data) {
					template = data;
				}
			});
			return template;
		}
});
