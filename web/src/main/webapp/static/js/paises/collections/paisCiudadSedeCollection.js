/**
 * Clase par el manejo de listas de ciudades
 */
var PaisCiudadSedeCollection = Backbone.Collection.extend({
	/**
	 * url en el servidor para manejar las peticiones rest/json generadas por backbone
	 */
	url : CONTEXT_PATH + '/rest/ciudades',
	/**
	 * Modelo asociado a esta Collection
	 */
    model: PaisCiudadSedeModel
});
