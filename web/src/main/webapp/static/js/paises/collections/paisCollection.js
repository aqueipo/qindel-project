/**
 * Clase par el manejo de listas de paises
 */
var PaisCollection = Backbone.Collection.extend({
	/**
	 * url en el servidor para manejar las peticiones rest/json generadas
	 * por backbone
	 */
	url : CONTEXT_PATH + '/rest/paises',
	/**
	 * Modelo asociado a esta Collection
	 */
    model: PaisModel
});
