/**
 * Esta clase representa la vista (y controlador) asociados a la lista de países.
 */
var PaisCollectionView = Backbone.View.extend({
	/**
	 * Elemento dom asociado a esta vista
	 */
	el : $('#contenido'),
	/**
	 * Plantilla html asociada a esta vista
	 */
	template : null,
	/**
	 * Plantilla html asociada a la vista de pais individual
	 */
	paisModelTemplate : null,
	/**
	 * Información de los libros
	 */
	paisCollection : null,

	/**
	 * Inicializador de la clase. Establece los atributos collection, template,
	 * libroModelTemplate, editarLibroTemplate
	 * y eliminarLibroTemplate
	 *
	 * @param options.
	 *            Es un hash con los siguientes elementos { collection : value,
	 *						template: value, paisModelTemplate: value }
	 */
	initialize : function(options) {

		this.template = options.template;
		this.paisModelTemplate = options.paisModelTemplate;
		this.paisCollection = options.collection;
		this.listenTo(this.paisCollection, 'reset', this.render);
	},

	/**
	 * Crea un nodo dom con la plantilla html (this.template) y los datos
	 * de los países (this.collection).
	 *
	 * @return Devuelve la instancia sobre la que se ejecuta la función.
	 */
	render : function() {
		this.recuperarPaises(this.renderCollection, this.error);
	},

	/**
	* Renderiza la colección
	*
	* @return Devuelve la instancia sobre la que se ejecuta la función.
	*/
	renderCollection : function() {
		$(this.el).html(this.template());
		_.each(this.collection.models, function(value) {
			$('#filas').append(new PaisModelView({
				model : value,
				template : this.paisModelTemplate,
			}).render().el);
		}, this);

		return this;
	},

	/**
	* Función que lanza alert si se produce error
	*/
	error : function() {
		alert('Se ha producido un error al recuperar la lista de paises.');
	},

	/**
	* Función que carga la colección
	*/
	recuperarPaises : function(success, error) {
		this.paisCollection.fetch({
			success : _.bind(success, this),
			error : _.bind(error, this)
		});
	},

	/**
	* Función que oculta el elemento dom
	*/
	hide : function() {
		$(this.el).empty();
	}
});
