/**
 * Renderiza una fila de la tabla de resultados, se corresponde con un modelo
 */
var PaisCiudadSedeModelView = Backbone.View.extend({
	/**
	 * Tag asociado a esta vista
	 */
	tagName : 'tr',
	/**
	 * Plantilla html asociada a la vista.
	 */
	template : null,

	/**
	 * Inicializador de la clase. Establece los atributos model, template,
	 * editarLibroView y eliminarLibroView
	 *
	 * @param options.
	 *            Es un hash con los siguientes elementos { model : value,
	 *						template: value }
	 */
	initialize : function(options) {
		this.template = options.template;
	},

	/**
	 * Crea un nodo dom con la plantilla html y datos de la ciudad (this.model).
	 *
	 * @return Devuelve la instancia sobre la que se ejecuta la función.
	 */
	render : function() {
		$(this.el).html(this.template(this.model.toJSON()));
		return this;
	},

});
