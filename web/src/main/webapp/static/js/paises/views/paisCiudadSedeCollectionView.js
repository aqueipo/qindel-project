/**
 * Esta clase representa la vista (y controlador) asociados
 * a la lista de ciudades.
 */
var PaisCiudadSedeCollectionView = Backbone.View.extend({
	/**
	 * Elemento dom asociado a esta vista
	 */
	el : $('#contenido'),
	/**
	 * Plantilla html asociada a esta vista
	 */
	template : null,
	/**
	 * Plantilla html asociada a la vista de pais individual
	 */
	paisCiudadSedeModelTemplate : null,
	/**
	 * Información de los libros
	 */
	paisCiudadSedeCollection : null,

	/**
	 * Inicializador de la clase. Establece los atributos collection, template,
	 * libroModelTemplate, editarLibroTemplate y eliminarLibroTemplate
	 *
	 * @param options.
	 *            Es un hash con los siguientes elementos { collection : value,
	 *						template: value, paisCiudadSedeModelTemplate: value }
	 */
	initialize : function(options) {

		this.template = options.template;
		this.paisCiudadSedeModelTemplate = options.paisCiudadSedeModelTemplate;
		this.paisCiudadSedeCollection = options.collection;
		this.listenTo(this.paisCiudadSedeCollection, 'reset', this.render);
	},

	/**
	 * Crea un nodo dom con la plantilla html (this.template) y los datos
	 * de las ciudades (this.collection).
	 *
	 * @return Devuelve la instancia sobre la que se ejecuta la función.
	 */
	render : function() {
		this.recuperarPaisesCiudadesSedes(this.renderCollection, this.error);
	},

	/**
	* Renderiza la colección
	*
	* @return Devuelve la instancia sobre la que se ejecuta la función.
	*/
	renderCollection : function() {
		$(this.el).html(this.template());
		_.each(this.collection.models, function(value) {
			$('#filas').append(new PaisCiudadSedeModelView({
				model : value,
				template : this.paisCiudadSedeModelTemplate,
			}).render().el);
		}, this);

		return this;
	},

	/**
	* Función que lanza alert si se produce error
	*/
	error : function() {
		alert('Se ha producido un error al recuperar la lista de ciudades.');
	},

	/**
	* Función que carga la colección
	*/
	recuperarPaisesCiudadesSedes : function(success, error) {
		this.paisCiudadSedeCollection.fetch({
			success : _.bind(success, this),
			error : _.bind(error, this)
		});
	},

	/**
	* Función que oculta el elemento dom
	*/
	hide : function() {
		$(this.el).empty();
	}
});
