/**
 * Modelo para ciudades con sus sedes y pais.
 */
var PaisCiudadSedeModel = Backbone.Model.extend({
	/**
	 * definiendo urlRoot no será necesario asociar a un modelo a ninguna
	 * colección para poder sincronizarlo
	 * con el servidor.
	 */
	urlRoot: CONTEXT_PATH + '/rest/ciudades',
	/**
	 * Indicamos a Backbone que utilice la propiedad idCiudad como identificador
	 * de la ciudad
	 */
	idAttribute: 'idCiudad',
	/**
	 * Lista de atributos mínimos que tendrán todas las instancias de
	 * PaisCiudadSedeModel.
	 */
	defaults: {
    idPais: null,
    nombrePais: '',
		idCiudad: null,
		nombreCiudad : '',
    valor: '',
    descripcionTipoJJOO: '',
    numeroVecesSede: 0
	}

});
