/**
 * Modelo para paises individuales.
 */
var PaisModel = Backbone.Model.extend({
	/**
	 * definiendo urlRoot no será necesario asociar a un modelo a ninguna colección para poder sincronizarlo
	 * con el servidor.
	 */
	urlRoot: CONTEXT_PATH + '/rest/paises',
	/**
	 * Indicamos a Backbone que utilice la propiedad idLibro como identificador del libro
	 */
	idAttribute: 'idPais',
	/**
	 * Lista de atributos mínimos que tendrán todas las instancias de LibroModel.
	 */
	defaults: {
		idPais: null,
		nombrePais : '',
		codigoPais : '',
    valorPais: 0
	}

});
