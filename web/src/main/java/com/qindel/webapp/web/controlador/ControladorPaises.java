package com.qindel.webapp.web.controlador;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qindel.webapp.modelo.dtos.PaisCiudadSedeDto;
import com.qindel.webapp.modelo.dtos.PaisDto;
import com.qindel.webapp.modelo.servicio.ServicioPaises;

/**
 * Esta clase toma el rol de controlador en Spring MVC e implementa todos los métodos que serán llamados desde la
 * interfaz web. Este controlador funciona recibiendo datos y devolviendo datos en formato JSON.
 *
 * @author richie
 *
 */
@Controller("controladorPaises")
public class ControladorPaises {

	/**
	 * Logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ControladorPaises.class);

	/**
	 * Instancia al servicio que implementa la lógica de negocio
	 */
	private ServicioPaises servicio;


	/**
	 * Método que devuelve la lista de todos los países de la base de datos.
	 * @return Una lista en json con todos los países dados de alta en el sistema.
	 */
	@RequestMapping(value = "/paises", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<PaisDto> findAllPaises() {
		LOGGER.debug("/paises");
		List<PaisDto> listaPaises = servicio.findAllPaises();
		return listaPaises;
	}

	/**
	 * Método que devuelve la lista de todos los países de la base de datos.
	 * @return Una lista en json con todos los países dados de alta en el sistema.
	 */
	@RequestMapping(value = "/ciudades", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<PaisCiudadSedeDto> findAllCiudades() {
		LOGGER.debug("/ciudades");
		List<PaisCiudadSedeDto> listaTest = servicio.findAllCiudades();
		return listaTest;
	}

	/**
	 * @return the servicio
	 */
	public ServicioPaises getServicio() {
		return servicio;
	}

	/**
	 * @param servicio the servicio to set
	 */
	public void setServicio(ServicioPaises servicio) {
		this.servicio = servicio;
	}


}