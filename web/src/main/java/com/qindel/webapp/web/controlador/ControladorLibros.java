package com.qindel.webapp.web.controlador;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.qindel.webapp.modelo.dtos.LibroDto;
import com.qindel.webapp.modelo.entities.Libro;
import com.qindel.webapp.modelo.servicio.ServicioLibros;

/**
 * Esta clase toma el rol de controlador en Spring MVC e implementa todos los métodos que serán llamados desde la
 * interfaz web. Este controlador funciona recibiendo datos y devolviendo datos en formato JSON.
 *
 * @author richie
 *
 */
@Controller("controladorLibros")
public class ControladorLibros {

	/**
	 * Logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ControladorLibros.class);

	/**
	 * Instancia al servicio que implementa la lógica de negocio
	 */
	private ServicioLibros servicio;

	/**
	 * Método asociado a la página de inicio.
	 *
	 * @return La página de inicio.
	 */
	@RequestMapping(value = "/index.html")
	public ModelAndView index() {
		LOGGER.debug("/index.html");
		ModelAndView modelAndView = new ModelAndView("/index.html");
		return modelAndView;
	}

	/**
	 * Método que devuelve la lista de todos los libros de la base de datos.
	 * @return Una lista en json con todos los libros dados de alta en el sistema.
	 */
	@RequestMapping(value = "/libros", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<LibroDto> findAllLibros() {
		LOGGER.debug("/libros");
		List<LibroDto> listaLibros = servicio.findAllLibros();
		return listaLibros;
	}
	
	/**
	 * Método que actualiza un libro en el sistema
	 * @param libro - El libro que se actualiza
	 * @return El libro actualizado en json
	 */
	@RequestMapping(value = "/libros", method = RequestMethod.POST)
	public @ResponseBody LibroDto addLibro(@RequestBody Libro libro) {
		LOGGER.debug("/libros");
		LibroDto _libro = new LibroDto();
		
		_libro.setTítulo(libro.getTitulo());
		_libro.setAutor(libro.getAutor());
		
		return servicio.addLibro(_libro);
		}

	/**
	 * Método que actualiza un libro en el sistema
	 * @param id - El identificador del libro
	 * @param libro - El libro que se actualiza
	 * @return El libro actualizado en json
	 */
	@RequestMapping(value = "/libros/{id}", method = RequestMethod.PUT)
	public @ResponseBody LibroDto updateLibro(@PathVariable("id") int id, @RequestBody Libro libro) {
		LOGGER.debug("/libros/{id}");
		LibroDto currentLibro = servicio.findLibro(id);
		
		currentLibro.setTítulo(libro.getTitulo());
		currentLibro.setAutor(libro.getAutor());
		
		return servicio.updateLibro(currentLibro);	
	}
	
	/**
	 * Método que elimina un libro de la lista de libros
	 * @param id - El identificador del libro a borrar
	 * @return El estado de la respuesta
	 */
	@RequestMapping(value = "/libros/{id}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<HttpStatus> deleteLibro(@PathVariable("id") int id) {
		LOGGER.debug("/libros/{id}");
		
		LibroDto currentLibro = servicio.findLibro(id);	
		servicio.deleteLibro(currentLibro);
		return new ResponseEntity<HttpStatus>(HttpStatus.OK);
	}

	/**
	 * @return the servicio
	 */
	public ServicioLibros getServicio() {
		return servicio;
	}

	/**
	 * @param servicio the servicio to set
	 */
	public void setServicio(ServicioLibros servicio) {
		this.servicio = servicio;
	}


}