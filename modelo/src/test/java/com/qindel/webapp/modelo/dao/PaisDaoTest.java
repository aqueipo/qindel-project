package com.qindel.webapp.modelo.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.qindel.webapp.modelo.entities.PaisDao;
import com.qindel.webapp.modelo.entities.Pais;

/**
 * Clase para crear los test en el Dao de paises
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-context.xml", "classpath:spring-test-context.xml" })
@TransactionConfiguration(defaultRollback = true, transactionManager = "txManager")
@Transactional
public class PaisDaoTest {

	/** El dao de paises. */
	@Autowired
	PaisDao paisdao;

	/**
	 * Verifica que la tabla de ciudades tiene alguna fila.
	 */
	@Test
	public void testNotEmptyCiudadesTable() {
		List<Object> lista = paisdao.findAllCiudades();
		Assert.assertTrue(!lista.isEmpty());
	}
	
	/**
	 * Verifica que la tabla de países tiene alguna fila.
	 */
	@Test
	public void testNotEmptyPaísesTable() {
		List<Pais> lista = paisdao.findAll();
		Assert.assertTrue(!lista.isEmpty());
	}

}