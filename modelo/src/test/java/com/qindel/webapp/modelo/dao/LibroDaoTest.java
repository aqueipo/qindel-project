package com.qindel.webapp.modelo.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.qindel.webapp.modelo.entities.Libro;
import com.qindel.webapp.modelo.entities.LibroDao;

/**
 * Clase para crear los test en el Dao de libros
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring-context.xml", "classpath:spring-test-context.xml" })
@TransactionConfiguration(defaultRollback = true, transactionManager = "txManager")
@Transactional
public class LibroDaoTest {

	/** El dao de libros. */
	@Autowired
	LibroDao libroDao;

	/**
	 * Verifica que la tabla de libros tiene alguna fila.
	 */
	@Test
	public void testNotEmptyLibroTable() {
		List<Libro> libroList = libroDao.findAll();
		Assert.assertTrue(!libroList.isEmpty());
	}
	
	/**
	 * Verifica que la tabla de libros tiene 9 elementos.
	 */
	@Test
	public void testSizeLibroTable() {
		List<Libro> libroList = libroDao.findAll();
		Assert.assertTrue(libroList.size() == 9);
	}
	
	/**
	 * Verifica la actualización de un libro.
	 */
	@Test
	public void testUpdateLibro() {
		
		// Verificamos Romeo y Julieta de William Shakespeare --> idLibro = 1
		Libro libro = libroDao.findLibro(1);
		String autorOriginal = libro.getAutor();
		libro.setAutor("Rosalía de Castro");
		Libro libroActualizado = libroDao.save(libro);

		Assert.assertTrue(!libroActualizado.getAutor().equals(autorOriginal));
	}
	
	/**
	 * Verifica la eliminación de un libro
	 */
	@Test
	public void testDeleteLibro() {
		// Eliminamos un libro
		List<Libro> libroList = libroDao.findAll();
		int originalSize = libroList.size();
		Libro libro = libroList.get(libroList.size() - 1);
		int idLibro = libro.getIdLibro();
		
		libroDao.delete(libro);
		
		int newSize = libroDao.findAll().size();
		libro = libroDao.findLibro(idLibro);
		
		Assert.assertTrue(newSize == originalSize - 1 && libro == null);
	}
	
	/**
	 * Verifica la creación de un libro
	 */
	@Test
	public void testCrearLibro() {
		// Eliminamos un libro
		List<Libro> libroList = libroDao.findAll();
		int originalSize = libroList.size();
		Libro libro = new Libro();
		libro.setTitulo("tituloTest");
		libro.setAutor("autorTest");
		
		libro = libroDao.add(libro);
		
		libro = libroDao.findLibro(libro.getIdLibro());
		
		int newSize = libroDao.findAll().size();
		
		Assert.assertTrue(newSize == originalSize + 1 && libro != null);
	}

}
