package com.qindel.webapp.modelo.dtos;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Esta clase guarda los datos referentes a un país concreto y se utiliza para intercambiar datos de países entre
 * distintas capas.
 *
 * @author richie
 *
 */
public class PaisCiudadSedeDto implements Serializable {

	private static final long serialVersionUID = 2L;

	/** Identificador del país. */
	private Integer idPais;

	/** Nombre del país. */
	private String nombrePais;

	/** Identificador de ciudad */
	private Integer idCiudad;

	/** Nombre de la ciudad. */
	private String nombreCiudad;
	
	/** Descripción de los juegos */
	private String descripcionTipoJJOO;
	
	/** Número de veces que fue sede */
	private Integer numeroVecesSede;
	
	/** Valor de la ciudad */
	private Integer valor;

	/**
	 * Constructor por defecto.
	 */
	public PaisCiudadSedeDto() {
		super();
	}

	/**
	 * Constructor que inicializa todos los atributos de la clase.
	 *
	 * @param idPais
	 *            identificador del país.
	 * @param nombrePais
	 *            nombre del país.
	 * @param idCiudad
	 *            identificador de la ciudad.
	 * @param nombreCiudad
	 *            nombre de la ciudad.
	 * @param valor
	 *            valor de la ciudad.
	 * @param descripcionTipoJJOO
	 *            descripción del tipo de JJOO.
	 * @param numeroVecesSede
	 *            numero de veces que la ciudad fue sede.
	 */
	public PaisCiudadSedeDto(
			Integer idPais, 
			String nombrePais, 
			Integer idCiudad,
			String nombreCiudad,
			Integer valor,
			String descripcionTipoJJOO,
			Integer numeroVecesSede) {
		
		this.idPais = idPais;
		this.nombrePais = nombrePais;
		this.idCiudad = idCiudad;
		this.nombreCiudad = nombreCiudad;
		this.valor = valor;
		this.descripcionTipoJJOO = descripcionTipoJJOO;
		this.numeroVecesSede = numeroVecesSede;
	}

	/**
	 * Gets the id pais.
	 *
	 * @return el identificador del país
	 */
	public Integer getIdPais() {
		return idPais;
	}

	/**
	 * Sets the id pais.
	 *
	 * @param idPais
	 *            el nuevo identificador del país
	 */
	public void setIdPais(Integer idPais) {
		this.idPais = idPais;
	}

	/**
	 * Gets the nombre pais.
	 *
	 * @return the nombrePais
	 */
	public String getNombrePais() {
		return nombrePais;
	}

	/**
	 * Sets the nombre pais.
	 *
	 * @param nombrePais
	 *            the nombrePais to set
	 */
	public void setNombrePais(String nombrePais) {
		this.nombrePais = nombrePais;
	}

	/**
	 * Gets the id ciudad
	 * 
	 * @return the idCiudad
	 */
	public Integer getIdCiudad() {
		return idCiudad;
	}

	/**
	 * Sets the id ciudad
	 * 
	 * @param idCiudad
	 */
	public void setIdCiudad(Integer idCiudad) {
		this.idCiudad = idCiudad;
	}

	/**
	 * Gets the nombre ciudad
	 * 
	 * @return the nombreCiudad
	 */
	public String getNombreCiudad() {
		return nombreCiudad;
	}

	/**
	 * Sets the nombre ciudad
	 * 
	 * @param nombreCiudad
	 */
	public void setNombreCiudad(String nombreCiudad) {
		this.nombreCiudad = nombreCiudad;
	}
	
	/**
	 * Gets the descripcion tipo JJOO
	 * 
	 * @return the descripcionTipoJJOO
	 */
	public String getDescripcionTipoJJOO() {
		return descripcionTipoJJOO;
	}

	/**
	 * Sets the descripcion tipo JJOO
	 * 
	 * @param descripcionTipoJJOO
	 */
	public void setDescripcionTipoJJOO(String descripcionTipoJJOO) {
		this.descripcionTipoJJOO = descripcionTipoJJOO;
	}

	/**
	 * Gets the numero veces sede
	 * 
	 * @return the numeroVecesSede
	 */
	public Integer getNumeroVecesSede() {
		return numeroVecesSede;
	}

	/**
	 * Sets the numero veces sede
	 * 
	 * @param numeroVecesSede
	 */
	public void setNumeroVecesSede(Integer numeroVecesSede) {
		this.numeroVecesSede = numeroVecesSede;
	}

	/**
	 * Gets the valor
	 * 
	 * @return the valor
	 */
	public Integer getValor() {
		return valor;
	}

	/**
	 * Sets the valor
	 * 
	 * @param valor
	 */
	public void setValor(Integer valor) {
		this.valor = valor;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.idPais).toHashCode();
	}

	/**
	 * Dos ciudades son iguales si tienen el mismo identificador.
	 *
	 * @param obj
	 *            the obj
	 * @return true, if successful
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaisCiudadSedeDto other = (PaisCiudadSedeDto) obj;
		return new EqualsBuilder().append(this.idCiudad, other.idCiudad).isEquals();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PaisCiudadSedeDto [idPais=").append(idPais).append(", nombrePais=").append(nombrePais)
				.append(", idCiudad=").append(idCiudad).append(", nombreCiudad=").append(nombreCiudad)
				.append(", valor=").append(valor).append(", numeroVecesSede=").append(numeroVecesSede).append("]");
		return builder.toString();
	}

}