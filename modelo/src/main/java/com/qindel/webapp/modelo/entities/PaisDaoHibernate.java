package com.qindel.webapp.modelo.entities;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class PaisDaoHibernate extends HibernateDaoSupport implements PaisDao {

	@Override
	public Pais findPais(int idPais) {
		return (Pais) getSessionFactory().getCurrentSession().get(Pais.class, idPais);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pais> findAll() {
		return getSessionFactory().getCurrentSession().createQuery("from Pais").list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object> findAllCiudades() {
		StringBuilder sb = new StringBuilder();
		sb.append("select pais.idPais, pais.nombrePais, pais.valorPais, ciudad.idCiudad, ciudad.nombreCiudad, ciudad.valorCiudad, tipo.idTipoJJOO, tipo.description, count(tipo.idTipoJJOO)")
			.append(" from Pais pais ")
			.append(" join pais.ciudades ciudad")
			.append(" left join ciudad.sedes sede")
			.append(" left join sede.tipoJJOO tipo")
			.append(" group by pais.idPais, pais.nombrePais, pais.valorPais, ciudad.idCiudad, ciudad.nombreCiudad, ciudad.valorCiudad, tipo.idTipoJJOO, tipo.description, tipo.idTipoJJOO");
		String query2 = sb.toString();
		return getSessionFactory().getCurrentSession().createQuery(query2).list();
	}

}
