package com.qindel.webapp.modelo.entities;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonManagedReference;;

/**
 * Clase entity asociada a la tabla aplicacion_web.pais. Guarda toda la información relativa a un país.
 *
 * @author richie
 *
 */
public class Sede implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2L;

	/** Año de la sede */
	private Integer anho;

	/** Referencia al tipo de JJOO */
	@JsonManagedReference
	private TipoJJOO tipoJJOO;
	
	/** Valor asignado a la ciudad. */
	@JsonManagedReference
	private Ciudad ciudad;
	
	/**
	 * Constructor por defecto.
	 */
	public Sede() {
		super();
	}

	/**
	 * Constructor que inicializa todos los atributos de la clase.
	 *
	 * @param anho
	 *            año en que una ciudad fue sede.
	 */
	public Sede(Integer anho) {
		this.anho = anho;
	}
	
	/**
	 * Gets the ciudad.
	 * 
	 * @return the ciudad
	 */
	public Ciudad getCiudad() {
		return ciudad;
	}
	
	/**
	 * Sets the ciudad.
	 * 
	 * @param ciudad
	 *            the ciudad to set
	 */
	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	/**
	 * Gets the anho
	 * 
	 * @return the anho
	 */
	public Integer getAnho() {
		return anho;
	}

	/**
	 * Sets the anho.
	 * 
	 * @param anho
	 *            the anho to set
	 */
	public void setAnho(Integer anho) {
		this.anho = anho;
	}
	
	/**
	 * Gets the tipoJJOO
	 * 
	 * @return the tipoJJOO
	 */
	public TipoJJOO getTipoJJOO() {
		return tipoJJOO;
	}

	/**
	 * Sets the tipoJJOO.
	 * 
	 * @param tipoJJOO
	 *            the tipoJJOO to set
	 */
	public void setTipoJJOO(TipoJJOO tipoJJOO) {
		this.tipoJJOO = tipoJJOO;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.anho).toHashCode();
		//return new HashCodeBuilder().append(this.anho).toHashCode();
	}

	/**
	 * Dos países son iguales si tienen el mismo identificador.
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sede other = (Sede) obj;
		boolean sameAnho = new EqualsBuilder().append(this.anho, other.anho).isEquals();
		boolean sameTipo = new EqualsBuilder().append(this.tipoJJOO.getIdTipoJJOO(), other.tipoJJOO.getIdTipoJJOO()).isEquals();
		return (sameAnho && sameTipo);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Ciudad [anho=").append(anho).append("]");
		return builder.toString();
	}

}