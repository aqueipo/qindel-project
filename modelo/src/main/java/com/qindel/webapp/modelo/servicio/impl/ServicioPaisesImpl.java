package com.qindel.webapp.modelo.servicio.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qindel.webapp.modelo.dtos.PaisCiudadSedeDto;
import com.qindel.webapp.modelo.dtos.PaisDto;
import com.qindel.webapp.modelo.entities.Pais;
import com.qindel.webapp.modelo.entities.PaisDao;
import com.qindel.webapp.modelo.servicio.ServicioPaises;

/**
 * Esta clase se encarga de la implementación del servicio web.
 *
 * @author richie
 *
 */
@Service("servicioPaisesImpl")
public class ServicioPaisesImpl implements ServicioPaises {

	private PaisDao paisDao;

	/**
	 * Logger.
	 */
	private final Logger log = LoggerFactory.getLogger(ServicioPaisesImpl.class);

	@Override
	@Transactional(readOnly = true)
	public List<PaisDto> findAllPaises() {
		List<Pais> paises = paisDao.findAll();
		this.log.debug("paises: " + paises);
		List<PaisDto> paisesDto = new ArrayList<PaisDto>(paises.size());
		for (Pais pais : paises) {
			paisesDto
					.add(new PaisDto(pais.getIdPais(), pais.getNombrePais(), pais.getCodigoPais(), pais.getValorPais()));
		}
		return paisesDto;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<PaisCiudadSedeDto> findAllCiudades() {
		
		List<Object> objs = paisDao.findAllCiudades();
		List<PaisCiudadSedeDto> ciudadesDto = new ArrayList<PaisCiudadSedeDto>(objs.size());

		for (Object obj : objs) {
			Object[] o = (Object[]) obj;
	        Integer idPais =  Integer.valueOf(String.valueOf(o[0]));
	        String nombrePais =  String.valueOf(o[1]);
	        Integer valorPais = Integer.valueOf(String.valueOf(o[2]));
	        Integer idCiudad =  Integer.valueOf(String.valueOf(o[3]));
	        String nombreCiudad = String.valueOf(o[4]);
	        Integer valorCiudad = String.valueOf(o[5]) == "null" ? valorPais : Integer.valueOf(String.valueOf(o[5]));
	        String tipoJJOO = String.valueOf(o[6]) == "null" ? "---" : String.valueOf(o[6]);
	        String descripcionTipoJJOO = String.valueOf(o[7]) == "null" ? "---" : String.valueOf(o[7]);
	        Integer numeroVeces = Integer.valueOf(String.valueOf(o[8]));
	        System.out.println("--------------------------------------------------------" +
	        "---------------------------------------------------------------------------");
	        System.out.println("| idPais: " + idPais 
	        		+ " | nombrePais: " + nombrePais + " | valorPais: " + valorPais
	        		+ " | idCiudad: " + idCiudad + " | nombreCiudad: " + nombreCiudad 
	        		+ " | valorCiudad: " + valorCiudad + " | tipoJJOO: " + tipoJJOO 
	        		+ " | descripcion:" + descripcionTipoJJOO + " | numeroVeces: " + numeroVeces 
	        		+ " |");
	        
	        ciudadesDto
	        		.add(new PaisCiudadSedeDto(idPais, nombrePais, idCiudad, nombreCiudad, valorCiudad, descripcionTipoJJOO, numeroVeces));

		}
		

		return ciudadesDto;

	}

	/**
	 * @return the paisDao
	 */
	public PaisDao getPaisDao() {
		return paisDao;
	}

	/**
	 * @param paisDao
	 *            the paisDao to set
	 */
	public void setPaisDao(PaisDao paisDao) {
		this.paisDao = paisDao;
	}

}