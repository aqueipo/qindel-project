package com.qindel.webapp.modelo.servicio.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qindel.webapp.modelo.dtos.LibroDto;
import com.qindel.webapp.modelo.entities.Libro;
import com.qindel.webapp.modelo.entities.LibroDao;
import com.qindel.webapp.modelo.servicio.ServicioLibros;

/**
 * Esta clase se encarga de la implementación del servicio de libros web.
 *
 * @author richie
 *
 */
@Service("servicioLibrosImpl")
public class ServicioLibrosImpl implements ServicioLibros {

	private LibroDao libroDao;

	/**
	 * Logger.
	 */
	private final Logger log = LoggerFactory.getLogger(ServicioLibrosImpl.class);

	@Override
	@Transactional(readOnly = true)
	public List<LibroDto> findAllLibros() {
		List<Libro> libros = libroDao.findAll();
		this.log.debug("libros antes: " + libros);
		Collections.sort(libros, new Comparator<Libro>() {
			@Override
			public int compare(Libro o1, Libro o2) {
				return o1.getTitulo().compareTo(o2.getTitulo());
			}
		});
		this.log.debug("libros despues: " + libros);
		List<LibroDto> librosDto = new ArrayList<LibroDto>(libros.size());
		for (Libro libro : libros) {
			librosDto.add(new LibroDto(libro.getIdLibro(), libro.getTitulo(), libro.getAutor()));
		}
		return librosDto;
	}
	
	@Override
	@Transactional(readOnly = true)
	public LibroDto findLibro(int idLibro) {
		Libro libro = libroDao.findLibro(idLibro);
		this.log.debug("libro: " + libro);
		LibroDto libroDto = new LibroDto(libro.getIdLibro(), libro.getTitulo(), libro.getAutor());

		return libroDto;
	}
	
	@Override
	@Transactional
	public LibroDto addLibro(LibroDto libro) {
		Libro _libro = new Libro(libro.getIdLibro(), libro.getTitulo(), libro.getAutor());
		_libro = libroDao.add(_libro);
		LibroDto result = new LibroDto(_libro.getIdLibro(), _libro.getTitulo(), _libro.getAutor());
		
		return result;
	}
	
	@Override
	@Transactional
	public LibroDto updateLibro(LibroDto libro) {
		Libro _libro = new Libro(libro.getIdLibro(), libro.getTitulo(), libro.getAutor());
		_libro = libroDao.save(_libro);
		LibroDto result = new LibroDto(_libro.getIdLibro(), _libro.getTitulo(), _libro.getAutor());
		
		return result;
	}
	
	@Override
	@Transactional
	public void deleteLibro(LibroDto libro) {
		Libro _libro = new Libro(libro.getIdLibro(), libro.getTitulo(), libro.getAutor());
		libroDao.delete(_libro);
	}

	/**
	 * @return the libroDao
	 */
	public LibroDao getLibroDao() {
		return libroDao;
	}

	/**
	 * @param libroDao
	 *            the libroDao to set
	 */
	public void setLibroDao(LibroDao libroDao) {
		this.libroDao = libroDao;
	}

}