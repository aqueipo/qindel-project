package com.qindel.webapp.modelo.entities;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class LibroDaoHibernate extends HibernateDaoSupport implements LibroDao {

	@Override
	public Libro findLibro(int idLibro) {
		return (Libro) getSessionFactory().getCurrentSession().get(Libro.class, idLibro);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Libro> findAll() {
		return getSessionFactory().getCurrentSession().createQuery("from Libro").list();
	}
	
	@Override
	public Libro save(Libro libro) {
		getSessionFactory().getCurrentSession().update(libro);
		return (Libro) getSessionFactory().getCurrentSession().get(Libro.class, libro.getIdLibro().intValue());
	}
	
	@Override
	public Libro add(Libro libro) {
		getSessionFactory().getCurrentSession().save(libro);
		return (Libro) getSessionFactory().getCurrentSession().get(Libro.class, libro.getIdLibro().intValue());
	}
	
	@Override
	public void delete(Libro libro) {
		getSessionFactory().getCurrentSession().delete(libro);
	}

}
