package com.qindel.webapp.modelo.servicio;

import java.util.List;
import com.qindel.webapp.modelo.dtos.PaisCiudadSedeDto;
import com.qindel.webapp.modelo.dtos.PaisDto;

/**
 * Esta interfaz define el servicio y los métodos que ofrece al mundo. 
 * {@link #findAllPaises() findAllPaises} método.
 * {@link #findAllCiudades() findAllCiudades} método.
 *
 */
public interface ServicioPaises {

	/**
	 * Este método se encarga de listar todos los países dados de alta en el sistema sin 
	 * ningún tipo de filtro.
	 * 
	 * @return La lista de países existentes en el sistema.
	 */
	public List<PaisDto> findAllPaises();
	
	/**
	 * Este método se encarga de listar todos las ciudades dados de alta en el sistema 
	 * relacionadas con sus países y sedes.
	 * 
	 * @return La lista de países existentes en el sistema.
	 */
	public List<PaisCiudadSedeDto> findAllCiudades();

}