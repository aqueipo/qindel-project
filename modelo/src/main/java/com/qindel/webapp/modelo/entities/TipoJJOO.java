package com.qindel.webapp.modelo.entities;

import java.io.Serializable;
import java.util.Set;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonBackReference;

/**
 * Clase entity asociada a la tabla aplicacion_web.pais. Guarda toda la información relativa a un país.
 *
 * @author richie
 *
 */
public class TipoJJOO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2L;

	/** Identificador de tipo de JJOO. */
	private Integer idTipoJJOO;

	/** Descripción del tipo de JJOO */
	private String description;
	
	/** Referencia a las sedes con el mismo tipo de JJOO. */
	@JsonBackReference
	private Set<Sede> sedes;
	
	/**
	 * Constructor por defecto.
	 */
	public TipoJJOO() {
		super();
	}

	/**
	 * Constructor que inicializa todos los atributos de la clase.
	 *
	 * @param idTipoJJOO
	 *            identificador de TipoJJOO.
	 * @param description
	 *            descripcion del tipo de JJOO.
	 */
	public TipoJJOO(Integer idTipoJJOO, String description) {
		this.idTipoJJOO = idTipoJJOO;
		this.description = description;
	}
	
	/**
	 * Gets the idTipoJJOO
	 * 
	 * @return the idTipoJJOO
	 */
	public Integer getIdTipoJJOO() {
		return idTipoJJOO;
	}

	/**
	 * Sets the idTipoJJOO.
	 * 
	 * @param idTipoJJOO
	 *            the idTipoJJOO to set
	 */
	public void setIdTipoJJOO(Integer idTipoJJOO) {
		this.idTipoJJOO = idTipoJJOO;
	}

	/**
	 * Gets the description
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the sedes
	 * 
	 * @return the sedes
	 */
	public Set<Sede> getSedes() {
		return sedes;
	}

	/**
	 * Sets the sedes.
	 * 
	 * @param sedes
	 *            the sedes to set
	 */
	public void setSedes(Set<Sede> sedes) {
		this.sedes = sedes;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.idTipoJJOO).toHashCode();
		//return new HashCodeBuilder().append(this.anho).toHashCode();
	}

	/**
	 * Dos países son iguales si tienen el mismo identificador.
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoJJOO other = (TipoJJOO) obj;
		return new EqualsBuilder().append(this.idTipoJJOO, other.idTipoJJOO).isEquals();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Ciudad [idTipoJJOO=").append(idTipoJJOO).append("]");
		return builder.toString();
	}

}