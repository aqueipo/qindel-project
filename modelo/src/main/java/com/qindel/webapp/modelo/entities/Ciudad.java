package com.qindel.webapp.modelo.entities;

import java.io.Serializable;
import java.util.Set;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonManagedReference;;

/**
 * Clase entity asociada a la tabla aplicacion_web.ciudad. Guarda toda la información relativa a un país.
 *
 * @author Adrián Queipo Pardo
 *
 */
public class Ciudad implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2L;

	/** Identificador de ciudad. */
	private Integer idCiudad;

	/** Nombre de la ciudad. */
	private String nombreCiudad;

	/** Valor asignado a la ciudad. */
	private Integer valorCiudad;
	
	/** Referencia al pais */
	@JsonManagedReference
	private Pais pais;
	
	/** Referencia a las sedes*/
	@JsonBackReference
	private Set<Sede> sedes;

	/**
	 * Constructor por defecto.
	 */
	public Ciudad() {
		super();
	}

	/**
	 * Constructor que inicializa todos los atributos de la clase.
	 *
	 * @param idCiudad
	 *            identificador del país.
	 * @param nombreCiudad
	 *            nombre del país.
	 * @param idPais
	 *            código ISO de dos caracteres del país.
	 * @param valorCiudad
	 *            valor del país.
	 */
	public Ciudad(Integer idCiudad, String nombreCiudad, Integer idPais, int valorCiudad) {
		this.idCiudad = idCiudad;
		this.nombreCiudad = nombreCiudad;
		//this.idPais = idPais;
		this.valorCiudad = valorCiudad;
	}

	/**
	 * Gets the id ciudad.
	 *
	 * @return the idCiudad
	 */
	public Integer getIdCiudad() {
		return idCiudad;
	}

	/**
	 * Sets the id ciudad.
	 *
	 * @param idCiudad
	 *            the idCiudad to set
	 */
	public void setIdCiudad(Integer idCiudad) {
		this.idCiudad = idCiudad;
	}

	/**
	 * Gets the nombre ciudad.
	 *
	 * @return the nombreCiudad
	 */
	public String getNombreCiudad() {
		return nombreCiudad;
	}

	/**
	 * Sets the nombre ciudad.
	 *
	 * @param nombreCiudad
	 *            the nombreCiudad to set
	 */
	public void setNombreCiudad(String nombreCiudad) {
		this.nombreCiudad = nombreCiudad;
	}

	/**
	 * Gets the valor ciudad.
	 *
	 * @return the valorCiudad
	 */
	public Integer getValorCiudad() {
		return valorCiudad;
	}

	/**
	 * Sets the valor ciudad.
	 *
	 * @param valorCiudad
	 *            the valorCiudad to set
	 */
	public void setValorCiudad(Integer valorCiudad) {
		this.valorCiudad = valorCiudad;
	}
	
	/**
	 * Gets the valor pais
	 * 
	 * @return the pais
	 */
	public Pais getPais() {
		return pais;
	}

	/**
	 * Sets the valor pais
	 * 
	 * @param pais
	 * 			the pais to set
	 */
	public void setPais(Pais pais) {
		this.pais = pais;
	}

	/**
	 * Gets the sedes
	 * 
	 * @return the sedes
	 */
	public Set<Sede> getSedes() {
		return sedes;
	}

	/**
	 * Sets the sedes
	 * 
	 * @param sedes
	 * 			the sedes to set
	 */
	public void setSedes(Set<Sede> sedes) {
		this.sedes = sedes;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.idCiudad).toHashCode();
	}

	/**
	 * Dos ciudades son iguales si tienen el mismo identificador.
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ciudad other = (Ciudad) obj;
		return new EqualsBuilder().append(this.idCiudad, other.idCiudad).isEquals();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Ciudad [idCiudad=").append(idCiudad).append(", nombreCiudad=").append(nombreCiudad)
				.append(", valorCiudad=").append(valorCiudad).append("]");
		return builder.toString();
	}

}
