package com.qindel.webapp.modelo.servicio;

import java.util.List;
import com.qindel.webapp.modelo.dtos.LibroDto;

/**
 * Esta interfaz define el servicio y los métodos que ofrece al mundo. 
 * {@link #findAllLibros() findAllLibros} método.
 * {@link #addLibro(Libro) addLibro} método.
 * {@link #deleteLibro(Libro) deleteLibro} método.
 * {@link #updateLibro(Libro) updateLibro} método.
 * {@link #findLibro(int) findLibro} método.
 *
 */
public interface ServicioLibros {

	/**
	 * Este método se encarga de listar todos los libros dados de alta en el sistema 
	 * sin ningún tipo de filtro.
	 * 
	 * @return La lista de libros existentes en el sistema.
	 */
	public List<LibroDto> findAllLibros();
	
	/**
	 * Este método se encarga de obtener un determinado libro
	 * 
	 * @param idLibro - El identificador de libro
	 * @return El libro solicitado
	 */
	public LibroDto findLibro(int idLibro);
	
	/**
	 * Este método se encarga de actualizar un determinado libro
	 * 
	 * @param libro - El libro a actualizar
	 * @return El libro actualizado
	 */
	public LibroDto addLibro(LibroDto libro);

	/**
	 * Este método se encarga de actualizar un determinado libro
	 * 
	 * @param libro - El libro a actualizar
	 * @return El libro actualizado
	 */
	public LibroDto updateLibro(LibroDto libro);
	
	/**
	 * Este método se encarga de borrar un libro del sistema
	 * 
	 * @param libro - El libro a borrar
	 */
	public void deleteLibro(LibroDto libro);
}