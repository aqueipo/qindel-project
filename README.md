# Prueba técnica para Qindel-group

Esto es una prueba técnica realizado con backbone, spring, hypersql y bootstrap

# Requisitos
 maven3 y un java sdk

# Instalacion
Obtención del código:
```shell
git clone https://gitlab.com/aqueipo/qindel-project.git
```

Ubicarse en el raíz del proyecto:
```shell
cd qindel-project
```

Ejecutar los siguientes comandos desde el raíz:
```shell
mvn clean install
cd web
mvn jetty:run
```


# Funcionalidades
* Listado de libros, edición, eliminación y creación
* Listado de países
* Listado de ciudades con sedes olímpicas

# Documentación
La documentación está disponible <a href="https://gitlab.com/aqueipo/qindel-project/wikis/home">aquí</a>